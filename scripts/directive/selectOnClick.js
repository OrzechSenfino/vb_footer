define([
	'jquery'
], function($) {
	var selectOnClick = [
		'$compile',
		function($compile) {
			return {
				link: function(scope, element, attrs) {
					element.on('click', function() {
						this.select();
					});
				}
			};
		}
	];

	return selectOnClick;
});