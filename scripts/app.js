requirejs.config({
	// waitSeconds: 2,
	baseUrl: './scripts',
	urlArgs: "bust=" + Math.random(),
	paths: {
		'jquery': '../bower_components/jquery/dist/jquery',
		'text': '../bower_components/requirejs-text/text'
	}
});

requirejs(
	[
		'angular',
		'jquery',
		'module/siteModule'
	],
	function(angular, $, siteModule) {
		$(function() {
			var htmlElement,
				inj;

			htmlElement = document.getElementsByTagName("html")[0];
			inj = angular.bootstrap(htmlElement, [siteModule.name]);
		});
	}
);