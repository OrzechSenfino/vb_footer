define(['text!mailTemplate.html'], function(mailTemplate) {
	var baseController = [
		'$scope',
		function($scope) {
			$scope.options = {
				name: 'Tomasz Niezgoda',
				position: 'CTO',
				mail: 'tomasz.niezgoda@senfino.com',
				phone: '+48792874635'
			};

			$scope.parsed = '';

			$scope.$watch('options', function(newValue) {
				var parsed = mailTemplate;

				parsed = parsed.replace('{{options.name}}', newValue.name);
				parsed = parsed.replace('{{options.position}}', newValue.position);
				parsed = parsed.replace('{{options.phone}}', newValue.phone);
				parsed = parsed.replace('{{options.phone}}', newValue.phone);
				parsed = parsed.replace('{{options.mail}}', newValue.mail);
				parsed = parsed.replace('{{options.mail}}', newValue.mail);

				$scope.parsed = parsed;
			}, true);
		}
	];

	return baseController;
});
