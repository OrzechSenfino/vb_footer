define([
	'angular',
	'controller/baseController',
	'directive/selectOnClick'
], function(
	angular,
	baseController,
	selectOnClick
) {
	var module = angular.module('siteModule', []);

	// controllers
	module.controller('baseController', baseController);

	// directive
	module.directive('selectOnClick', selectOnClick);

	return module;
});